---
layout: post
title: Test Jupyter 
subtitle: Test1 for me
cover-img: /assets/img/pobrane.png
thumbnail-img: /assets/img/1200px-Jupyter_logo.png
share-img: /assets/img/pobrane.png
tags: [jupyter, test]
---


Tutaj przykładowy kod: [some code I wrote for code](code.md).


```python
x = 1
if x == 1:
    # indented four spaces
    print("x is 1.")
```

    x is 1.



```python

```



Here's a code chunk:

~~~
var foo = function(x) {
  return(x + 5);
}
foo(3)
~~~

And here is the same code with syntax highlighting:

```javascript
var foo = function(x) {
  return(x + 5);
}
foo(3)
```

And here is the same code yet again but with line numbers:

{% highlight javascript linenos %}
var foo = function(x) {
  return(x + 5);
}
foo(3)
{% endhighlight %}

## Boxes
You can add notification, warning and error boxes like this:

### Notification

{: .box-note}
**Note:** This is a notification box.

### Warning

{: .box-warning}
**Warning:** This is a warning box.

### Error

{: .box-error}
**Error:** This is an error box.

